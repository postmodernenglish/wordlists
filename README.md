# WordLists #

This repository contains lists of all written Postmodern English words that have been approved by our team, listed in alphabetical order. Feel free to clone these word lists for your own usage. The public is not permitted to add to these lists directly- rather, 
the word audio files must be added to the WordBank repository first, and our team will then review and add the words to the lists here.